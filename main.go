package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"runtime"
	"strconv"
	"telunjuk-worker/utils"
	"telunjuk-worker/worker/consumers/services"
	_"telunjuk-worker/db"
)

var(
	port string
)

func main()  {


	maxProc, _ := strconv.Atoi(utils.GetEnv("MAXPROCS", "1"))
	port = utils.GetEnv("TELUNJUK_WORKER_PORT", "8002")
	runtime.GOMAXPROCS(maxProc)

	r := gin.New()
	r.Use(gin.Recovery())
	endPoint := fmt.Sprintf(":%d", port)

	log.Println("[info] start http server listening %s", endPoint)

	go services.StartServicesKafka()


	//server.ListenAndServe()

	r.Run(":" + port)

}

