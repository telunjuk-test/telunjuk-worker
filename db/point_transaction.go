package db

import "telunjuk-worker/models/dbmodels"

type PointTransactionRepo struct {

}

func InitPointTransactionRepo() *PointTransactionRepo {
	return &PointTransactionRepo{}
}

func (r *PointTransactionRepo) Save(req *dbmodels.PointTransaction)  error{
	db := GetDbCon()

	err := db.Save(&req).Error

	return err
}