package db

import (
	"log"
	"telunjuk-worker/models/dbmodels"
	"testing"
	"time"
)

func TestListUserRepo_FindByPhoneNumber(t *testing.T) {
	res, err := InitListUserRepo().FindByPhoneNumber("081394320721")
	log.Println(res, err)
}


func TestListUserRepo_Save(t *testing.T) {

	req:= dbmodels.ListUser{
		PhoneNumber:"0811222",
		Name: "abc",
		UpdatedAt: time.Now(),
		Point:  10,

	}
	err := InitListUserRepo().Save(&req)
	log.Println(req, err)
}