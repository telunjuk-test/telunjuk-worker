package db

import "telunjuk-worker/models/dbmodels"

type ListUserRepo struct {

}

func InitListUserRepo()  *ListUserRepo{
	return &ListUserRepo{}
}

func (r *ListUserRepo) FindByPhoneNumber(phoneNumber string) (dbmodels.ListUser, error) {
	db := GetDbCon()
	var listUser dbmodels.ListUser



	err := db.Where(dbmodels.ListUser{PhoneNumber:phoneNumber}).First(&listUser).Error

	return listUser, err

}


func (r *ListUserRepo) Save(req *dbmodels.ListUser)  error{
	db := GetDbCon()

	err := db.Save(&req).Error

	return err
}
