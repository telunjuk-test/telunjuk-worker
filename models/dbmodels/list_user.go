package dbmodels

import "time"

type ListUser struct {
	ID 				int64 `json:"id"`
	PhoneNumber		string `json:"phoneNumber"`
	Name 			string `json:"name"`
	Point 			int64 `json:"point"`
	UpdatedAt 		time.Time `json:"updatedAt"`
}

func (t *ListUser) TableName() string  {
	return "list_user"
}
