package dbmodels

import "time"

type PointTransaction struct {
	ID 				int64 `json:"id"`
	PhoneNumber		string `json:"phoneNumber"`
	Key 			string `json:"key"`
	Value 			int64 `json:"value"`
	UpdatedAt 		time.Time `json:"updatedAt"`
}

func (t *PointTransaction) TableName() string  {
	return "point_transaction"
}
