package services

import (
	"fmt"
	"github.com/astaxie/beego/logs"
	"github.com/bsm/sarama-cluster"
	"go.uber.org/zap"
	"log"
	"strconv"
	"strings"
	"telunjuk-worker/services"
	"telunjuk-worker/utils"
	"telunjuk-worker/worker/consumers/kafka"
	"telunjuk-worker/worker/consumers/models"
)

var (
	consumerGroup          string
	kafkaTopics            string
	zookeeper              string
	brokers                string
	kafkaTopicsSFAError     string
	offsetkafka            int
	sugarLogger *zap.Logger
)
func init()  {
	consumerGroup = utils.GetEnv("KAFKA_GROUP", "posting-point-group")
	brokers = utils.GetEnv("KAFKA_BROKERS", "localhost:9092,localhost:9093,localhost:9094")
	kafkaTopics = utils.GetEnv("KAFKA_TOPIC_SEND_POINT", "test001")
	zookeeper =utils.GetEnv("KAFKA_ZOOKEEPER", "localhost:2181")
	offsetkafka,_ = strconv.Atoi(utils.GetEnv("KAFKA_OFFSET", "1"))



}

func StartServicesKafka() {

	fmt.Println("Start Services Kafka")
	configKafka := models.ConfigKafka{}
	configKafka.ConsumerGroup = consumerGroup
	configKafka.KafkaTopics = strings.Split(kafkaTopics, ",")
	configKafka.Zookeeper = strings.Split(zookeeper, ",")
	configKafka.ConsumerConfig = kafka.GetConsumerConfig()

	config := cluster.NewConfig()
	config.Consumer.Return.Errors = true
	config.Group.Return.Notifications = true
	if offsetkafka == 0 {
		config.Consumer.Offsets.Initial = -2
	}

	listBroker := strings.Split(brokers, ",")
	topics := strings.Split(kafkaTopics, ",")
	consumer, err := cluster.NewConsumer(listBroker, consumerGroup, topics, config)

	if err != nil {
		log.Println("Cluster Connect problem ", err)
		//fmt.Println("Cluster Connect problem ", err)
	}
	// consume errors
	go func() {
		for err := range consumer.Errors() {
			log.Println("Consumer Error: %s\n", err)
			//err.Error()
		}
	}()

	// consume notifications
	go func() {
		for ntf := range consumer.Notifications() {
			logs.Info("Rebalanced: %+v\n", ntf)
		}
	}()

	// consume messages, watch signals
	for {
		select {
		case msg, ok := <-consumer.Messages():
			if ok {
				go services.InitPointService().Process(msg.Value)
				consumer.MarkOffset(msg, "") // mark message as processed
			}
		}
	}
}

