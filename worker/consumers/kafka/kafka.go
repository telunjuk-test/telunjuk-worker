package kafka

import (
	"github.com/Shopify/sarama"
	"github.com/wvanbergen/kafka/consumergroup"
	"telunjuk-worker/worker/consumers/models"
	"time"
)

func GetConsumerConfig() *consumergroup.Config {
	config := consumergroup.NewConfig()
	config.Offsets.Initial = sarama.OffsetNewest
	config.Offsets.ProcessingTimeout = 10 * time.Second
	return config
}

//Multiple Topic
func ConnectConsumer(cfgKafka models.ConfigKafka) (*consumergroup.ConsumerGroup, error) {
	config := cfgKafka.ConsumerConfig
	consumer, consumerErr := consumergroup.JoinConsumerGroup(cfgKafka.ConsumerGroup, cfgKafka.KafkaTopics, cfgKafka.Zookeeper, config)
	if consumerErr != nil {
		return nil, consumerErr
	}
	return consumer, nil
}
