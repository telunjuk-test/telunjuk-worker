package models

import (
	"github.com/wvanbergen/kafka/consumergroup"
)

type ConfigKafka struct {
	ConsumerGroup  string
	KafkaTopics    []string
	Zookeeper      []string
	ConsumerConfig *consumergroup.Config
}