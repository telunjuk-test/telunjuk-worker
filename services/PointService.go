package services

import (
	"encoding/json"
	"fmt"
	"log"
	"telunjuk-worker/db"
	"telunjuk-worker/models"
	"telunjuk-worker/models/dbmodels"
)

type PointService struct {
	ListUserRepo *db.ListUserRepo
	PointTransactionRepo *db.PointTransactionRepo
}

func InitPointService() *PointService {
	return &PointService{
		ListUserRepo: db.InitListUserRepo(),
		PointTransactionRepo: db.InitPointTransactionRepo(),
	}
}

func (s *PointService) Process(data []byte) error {
	fmt.Println("<< PointService -- Process >>")

	log.Println(string(data))

	var reqPoint models.ReqPoint
	if err := json.Unmarshal(data, &reqPoint); err != nil {
		fmt.Println("==============================")
		fmt.Println("data err--> ", string(data))
		fmt.Println("failed unmarshal reqPoint", err)
		fmt.Println("==============================")
		return  err
	}

	log.Println(reqPoint)
	//find user by phoneNumber
	user, err := s.ListUserRepo.FindByPhoneNumber(reqPoint.PhoneNumber)
	if err != nil {
		fmt.Println("err get user by phoneNumber", err)
		return err
	}

	pointTransaction := dbmodels.PointTransaction{
		Key: reqPoint.Key,
		Value: reqPoint.Value,
		PhoneNumber: reqPoint.PhoneNumber,
		UpdatedAt: reqPoint.TransactionTime,
	}

	// save to transaction
	if err := s.PointTransactionRepo.Save(&pointTransaction); err != nil {
		fmt.Println("err save pointTransaction, err)", err)
		return err
	}

	// add point
	user.Point = user.Point + reqPoint.Value
	user.UpdatedAt = reqPoint.TransactionTime
	if err:= s.ListUserRepo.Save(&user); err!= nil{
		fmt.Println("err save user, err)", err)
		return err
	}



	fmt.Println("--Finish--")



	return nil
}